Samples and Demos
#################


.. toctree::
   :maxdepth: 3
   :glob:

   kernel
   basic/*
   subsys/subsys.rst
   net/net.rst
   bluetooth/bluetooth.rst
   sensor/*
   grove/*
   advanced
   power/power.rst





To add a new sample documentation, please use the template available under
:file:`doc/templates/sample.tmpl`
